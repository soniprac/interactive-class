﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
    class Vehicle
    {
        protected string _make;

        protected double _speed;

        protected double _mileage;

        protected double _fuelCapacity;

        public Vehicle(string make)
        {
            //Initialize ALL field variables
            _make = make;
            _speed = 0d;
            _mileage = 0d;
            _fuelCapacity = 0d;
        }

        /// <summary>
        /// Property to access and change the make of the vehicle
        /// </summary>
        public string Make
        {
            get
            {
                //TODO: ensure the make has been initialized
                return _make;
            }

            set
            {
                //TODO: Validate value to make sure it is not empty
                _make = value;
            }
        }

        /// <summary>
        /// Provide acessor and mutator methods for the speed of the vehicle
        /// </summary>
        public double Speed
        {
            get => _speed;
            set => _speed = value;
        }


        public double Mileage
        {
            get => _mileage;
            set => _mileage = value;
        }

        public double FuelCapacity
        {
            get { return _fuelCapacity; }
            set { _fuelCapacity = value; }
        }

        public virtual void Go()
        {
            Console.WriteLine($"Generic vehicle of make {_make} going with speed {_speed}");
        }
    }
}
