﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
    class Car : Vehicle //Card IS-A Vehicle
    {
        public Car(string make):base(make)
        {
        }

        public Car() : base("no name car")
        {
        }
        

        public override void Go()
        {
            Console.WriteLine($"{_make} going vrummmm vrummmm with speed {_speed} km/h");
        }
    }
}
