﻿using System;

namespace Vehicles
{
    class Program
    {
        static void doWork()
        {
            //Create a base vehicle object (instance of class vehicle)
            Vehicle genVehicle = new Vehicle("Generic Vehicle");
            genVehicle.Speed = 60;
            Console.WriteLine($"The speed of {genVehicle.Make} is {genVehicle.Speed}");
            genVehicle.Go();

            //Create car object
            Car fastCar = new Car("Chevy Camaro");
            fastCar.Go();

            //Work with any vehicle
            Vehicle vehicle = null;

            vehicle = genVehicle;
            vehicle.Go();

            vehicle = fastCar;
            vehicle.Go();

            //TODO: create an Airplane and ask it to go
            
        }

        static void Main()
        {
            try
            {
                doWork();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }
        }
    }
}
